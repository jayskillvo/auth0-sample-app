(function () {

  'use strict';

  angular
    .module('app', ['auth0.auth0'])
    .config(config);

  config.$inject = [
    'angularAuth0Provider'
  ];

  function config(
    angularAuth0Provider
  ) {

    // Initialization for the angular-auth0 library
    angularAuth0Provider.init({
      clientID: 'enpmPtw1qJPwKS2TKw7U6TKRMrAGfphZ',
      domain: 'skillvo.auth0.com',
      responseType: 'token id_token',
      audience: 'https://skillvo.auth0.com/api/v2/',
      redirectUri: 'http://localhost:3000/angular-callback',
      scope: 'read:messages'
    });

    /// Comment out the line below to run the app
    // without HTML5 mode (will use hashes in routes)
  }

})();

(function () {

  'use strict';

  angular
    .module('app')
    .service('authService', authService);

  authService.$inject = ['angularAuth0', '$timeout'];

  function authService(angularAuth0, $timeout) {

    function login() {
      angularAuth0.authorize();
    }

    function handleAuthentication() {
      angularAuth0.parseHash(function(err, authResult) {
        if (authResult && authResult.accessToken && authResult.idToken) {
          setSession(authResult);
        } else if (err) {
          $timeout(function() {
          });
          console.log(err);
        }
      });
    }

    function setSession(authResult) {
      // Set the time that the access token will expire at
      let expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
      localStorage.setItem('access_token', authResult.accessToken);
      localStorage.setItem('id_token', authResult.idToken);
      localStorage.setItem('expires_at', expiresAt);
    }
    
    function logout() {
      // Remove tokens and expiry time from localStorage
      localStorage.removeItem('access_token');
      localStorage.removeItem('id_token');
      localStorage.removeItem('expires_at');
    }
    
    function isAuthenticated() {
      // Check whether the current time is past the 
      // access token's expiry time
      let expiresAt = JSON.parse(localStorage.getItem('expires_at'));
      return new Date().getTime() < expiresAt;
    }


    return {
      login: login,
      handleAuthentication: handleAuthentication,
      logout: logout,
      isAuthenticated: isAuthenticated
    }
  }
})();

(function() {
  
  'use strict';
  
  angular
    .module('app')
    .controller('loginCtrl', loginCtrl);
    
    loginCtrl.$inject = ['$scope', 'authService'];

    function loginCtrl($scope, authService) {
      $scope.auth = authService;
    }

})();