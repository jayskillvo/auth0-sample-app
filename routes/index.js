var express = require('express');
var router = express.Router();
const passport = require('passport');
var request = require("request");
const app = express();
const jwt = require('express-jwt');
const jwksRsa = require('jwks-rsa');
const jwtAuthz = require('express-jwt-authz');

const env = {
    AUTH0_CLIENT_ID: 'enpmPtw1qJPwKS2TKw7U6TKRMrAGfphZ',
    AUTH0_DOMAIN: 'skillvo.auth0.com',
    AUTH0_CALLBACK_URL: 'http://localhost:3000/callback'
};

const checkJwt = jwt({
  // Dynamically provide a signing key
  // based on the kid in the header and 
  // the signing keys provided by the JWKS endpoint.
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://skillvo.auth0.com/.well-known/jwks.json`
  }),

  // Validate the audience and the issuer.
  audience: 'https://skillvo.auth0.com/api/v2/',
  issuer: `https://skillvo.auth0.com/`,
  algorithms: ['RS256']
});

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});

router.get('/angular-callback', function(req, res, next) {
    res.render('angular-callback', { title: 'Express' });
});

/* GET home page. */
router.get('/angular', function(req, res, next) {
    res.render('angular', { title: 'Express' });
});


router.get('/login', passport.authenticate('auth0', {
        clientID: env.AUTH0_CLIENT_ID,
        domain: env.AUTH0_DOMAIN,
        redirectUri: env.AUTH0_CALLBACK_URL,
        audience: 'https://skillvo.auth0.com/userinfo',
        responseType: 'id_token',
        scope: 'read:messages'
    }),
    function(req, res) {
        res.redirect('/');
    }
);

router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/');
});

router.get('/callback', function(req, res, next) {
	console.log(req.query.code);
    var options = {
    	method: 'POST',
	  	url: 'https://skillvo.auth0.com/oauth/token',
	  	headers: { 'content-type': 'application/json' },
	  	body: { 
		   	grant_type: 'authorization_code',
		    client_id: 'acM8EanSl28NkEfLG52fxQCNUrR7v1jW',
		    client_secret: 'UzZisxrHxrC4zgH30kTS-3xa4TVTL3inB0hdDIM8KvnMVHFY0JHwj4AWkjqr7IM8',
		    code: req.query.code,
		    redirect_uri: 'http://localhost:3000/callback'
		},
	  	json: true 
	};

	request(options, function (error, response, tokens) {
	  if (error) {
	  	res.send(error);
	  	return;
	  };
	  if(tokens.access_token) {

	  	var options = {
	    	method: 'POST',
		  	url: 'https://skillvo.auth0.com/userinfo',
		  	headers: { 'Authorization': 'Bearer ' + tokens.access_token }
		};

		request(options, function (userInfoError, userResponse, user) {
			if (userInfoError) {
			  	res.send(userInfoError);
			  	return;
		  	};
	  		
	  		res.send({user: JSON.parse(user), tokens: tokens});
		});

	  }
	});
});

router.get('/api/public', function(req, res) {
  res.json({
    message: "Hello from a public endpoint! You don't need to be authenticated to see this."
  });
});

router.get('/api/private', checkJwt, function(req, res) {
  res.json({
    message: 'Hello from a private endpoint! You need to be authenticated and have a scope of read:messages to see this.'
  });
});


module.exports = router;